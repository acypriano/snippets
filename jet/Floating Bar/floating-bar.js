// This function show and hidden floating bar if position top of blocoValores is greater than 0
export function initFloatingBar () {
    // Select floating bar
    const flutuante = document.querySelector('.flutuante');
    // Select .blocoValores
    const blocoValores = document.querySelector('.blocoValores');

    // When window scroll
    document.addEventListener('scroll', () => {
        (blocoValores.getBoundingClientRect().top < 0) ? showFloating () : hideFloating ();

        // show floating bar
        function showFloating () {
            // hidden widget whatsapp
            document.querySelector('.wmt-plus').style.display = 'none';
            // add class to show floating
            flutuante.classList.add('ativo');
        }

        // hidden floating bar
        function hideFloating () {
            // show widget whatsapp
            document.querySelector('.wmt-plus').style.display = 'block';
            // remove class to hidden floating
            flutuante.classList.remove('ativo');
        }
    })
}