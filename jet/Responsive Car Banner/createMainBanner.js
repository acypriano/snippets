export function createMainBanner () {
    const mainBanner = document.querySelector('.main-banner');
    const carBanner = document.querySelectorAll('.car-banners a');
    const keyword = 'Mobile: ';

    // Init function, compare length of array's split, if 1 select images whithout keyword on title
    function selectImagesOfCarBanner (value) {
        carBanner.forEach(element => {
            (element.title.split('Mobile: ').length == value) ? createMainBanner (element) : null;
        })
        // Init slick after populate main banner 
        $('.main-banner').slick({
            arrows: false,
            dots: true,
            autoplay: true,
            autoplaySpeed: 5000
        });
        setTimeout(() => {
            window.scrollTo({
                top: 0,
                behavior: 'smooth'
            });
        }, 100);
    };

    // This function get attributes of element A and your firstElementChild <img> 
    function createMainBanner (element) {
        const objNewSlider = {
            urlSlider: element.href,
            urlImage: element.firstElementChild.getAttribute('data-src'),
            titleSlider: element.title.split('\\')[0],
            titleButton: element.title.split('\\')[1]
        }

        // create new content with infos to populate .main-banner
        const newContentSlider = document.createElement('div');
        newContentSlider.innerHTML = `
            <a href="${objNewSlider.urlSlider}" title="${objNewSlider.titleSlider}">
                <img src="${objNewSlider.urlImage}"/>
            </a>
        `;

        // populate .main-banner
        mainBanner.appendChild(newContentSlider);

        // Insert content info constructed with objNewSlider and target newContentSlider
        createContentInfoBanner (objNewSlider, newContentSlider);  
    };

    // create content with title and button from new Content
    function createContentInfoBanner (objNewSlider, newContentSlider) {
        const contentInfo = document.createElement('div');
        contentInfo.classList.add('content-info-slick');

        // inser infos into .content-info-slick
        contentInfo.innerHTML = `
            <h1>
                ${(objNewSlider.titleSlider.split(keyword).length > 1)
                ? objNewSlider.titleSlider.split(keyword)[1]
                : objNewSlider.titleSlider.split(keyword)[0]}
            </h1>
            <a href="${objNewSlider.urlSlider}">${objNewSlider.titleButton}</a>
        `;

        // insert content into element <a> of new slider
        newContentSlider.firstElementChild.appendChild(contentInfo);
    }

    /* Set number 2 to mobile and 1 to desktop,
    this number set value in array's split on params, if title contains the keyword 'Mobile: ', 
    array length == 2, else 1.  
    */
    (window.matchMedia("(max-width: 1000px)").matches) ? selectImagesOfCarBanner (2) : selectImagesOfCarBanner (1);
}   