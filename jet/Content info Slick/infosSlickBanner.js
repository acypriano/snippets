export function addButtonsSlickBanners () {
    // className's slicks banners
    const sliderSelected = ['car-banners'];

    // Loop for each banner
    sliderSelected .forEach(element => selectItem (element));

    function selectItem (classNameBanner) {
        // Select item no clone
        const slickItem = document.querySelectorAll(`.${classNameBanner} .slick-track > div`);

        slickItem.forEach(element => {
            (element.className == 'slick-slide slick-current slick-active' || element.className == 'slick-slide') ? createObjectInfosBanner (element): null;
        });
        
        // function to create object with infos
        function createObjectInfosBanner(element) {
            // Select element <a>
            const imageLink = element.childNodes[0].childNodes[0].childNodes[1].firstElementChild;

            // Object info
            const objContentInfo = {
                urlBanner: imageLink.getAttribute('href'),
                // Split infos on title of image between '\'
                titleBanner: imageLink.title.split('\\')[0],
                titleButton: imageLink.title.split('\\')[1]
            }
            imageLink.title = objContentInfo.titleBanner;
            imageLink.firstElementChild.title = objContentInfo.titleBanner;
            imageLink.firstElementChild.alt = objContentInfo.titleBanner;

            createContentInfo(objContentInfo, element);

            // Create content info
            function createContentInfo (obj, element) {
                // create content info
                const contentInfo = document.createElement('div');
                contentInfo.classList.add('content-info-slick');
                
                // create title
                const title = document.createElement('h1');
                title.textContent = obj.titleBanner;
                contentInfo.appendChild(title);
                
                // create button
                const button = document.createElement('a');
                button.setAttribute('href', obj.urlBanner);
                button.textContent = obj.titleButton;
                contentInfo.appendChild(button);

                // Insert contentInfo in slider
                element.appendChild(contentInfo)

            }
            
        }
    }
}
